package lab.grafica.solarsystem;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.util.glu.GLU.gluOrtho2D;

public class Main {

    private static final float EARTH_RADIUS = 8;
    private static final float EARTH_AU = 70;
    private static final float EARTH_ORBIT = 365f;

    public static void main(String[] args) throws LWJGLException {
        Display.setDisplayMode(new DisplayMode(800, 600));
        Display.create();

        gluOrtho2D(-400, 400, -300, 300);

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glClearColor(0, 0, 0, 0);

        /*CelestialBody sun = new CelestialBody(109f * EARTH_RADIUS, 0xffff6611);
        CelestialBody mercury = sun.addOrbiter(0.3829f * EARTH_RADIUS, 0.4f * EARTH_AU, 2, 0xffff0000);
        CelestialBody venus = sun.addOrbiter(0.9499f * EARTH_RADIUS, 0.7f * EARTH_AU, 1.5f, 0xffffaabb);
        CelestialBody earth = sun.addOrbiter(EARTH_RADIUS, EARTH_AU, 1, 0xff00ccaa);
        CelestialBody moon = earth.addOrbiter(0.273f * EARTH_RADIUS, 1, 0.5f, 0xffffffff);
        CelestialBody mars = sun.addOrbiter(0.533f * EARTH_RADIUS, 1.5f * EARTH_AU, 0.8f, 0xffff5533);
        CelestialBody jupiter = sun.addOrbiter(11.209f * EARTH_RADIUS, 5.2f * EARTH_AU, 0.1f, 0xffff8888);
        CelestialBody saturn = sun.addOrbiter(9.4492f * EARTH_RADIUS, 9.5f * EARTH_AU, 0.1f, 0xffffff00);
        CelestialBody uranus = sun.addOrbiter(4.007f * EARTH_RADIUS, 19.6f * EARTH_AU, 0.1f, 0xff4499aa);
        CelestialBody neptune = sun.addOrbiter(3.883f * EARTH_RADIUS, 30f * EARTH_AU, 0.1f, 0xff4455aa);*/

        CelestialBody sun = new CelestialBody(6f * EARTH_RADIUS, 0xffff6611);
        CelestialBody mercury = sun.addOrbiter(0.3829f * EARTH_RADIUS, 0.4f * EARTH_AU, EARTH_ORBIT / 88, 0xffff0000);
        CelestialBody venus = sun.addOrbiter(0.9499f * EARTH_RADIUS, 0.7f * EARTH_AU, EARTH_ORBIT / 224.7f, 0xffffaabb);
        CelestialBody earth = sun.addOrbiter(EARTH_RADIUS, EARTH_AU, 1, 0xff00ccaa);
        CelestialBody moon = earth.addOrbiter(0.273f * EARTH_RADIUS, 1, 0.5f, 0xffffffff);
        CelestialBody mars = sun.addOrbiter(0.533f * EARTH_RADIUS, 1.25f * EARTH_AU, EARTH_ORBIT / 686.971f, 0xffff5533);
        CelestialBody jupiter = sun.addOrbiter(3.2f * EARTH_RADIUS, 2f * EARTH_AU, EARTH_ORBIT / 4332.59f, 0xffff8888);
        CelestialBody saturn = sun.addOrbiter(2.4f * EARTH_RADIUS, 2.7f * EARTH_AU, EARTH_ORBIT / 10759.22f, 0xffffff00);
        CelestialBody uranus = sun.addOrbiter(1.3f * EARTH_RADIUS, 3.2f * EARTH_AU, EARTH_ORBIT / 30799.095f, 0xff4499aa);
        CelestialBody neptune = sun.addOrbiter(1.1f * EARTH_RADIUS, 3.6f * EARTH_AU, EARTH_ORBIT / 60190.03f, 0xff4455aa);

        long time = System.currentTimeMillis();
        while (!Display.isCloseRequested()) {
            long now = System.currentTimeMillis();
            sun.move(now - time / 1000f);
            time = now;

            glClear(GL_COLOR_BUFFER_BIT);

            sun.draw();

            Display.update();
            Display.sync(60);
        }
    }

}
