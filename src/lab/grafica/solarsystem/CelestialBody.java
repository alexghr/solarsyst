package lab.grafica.solarsystem;

import lab.grafica.geometry.Circle;
import lab.grafica.geometry.Point;
import lab.grafica.logic.Movable;
import lab.grafica.ui.Disk;
import lab.grafica.ui.Drawable;

import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;

public class CelestialBody implements Drawable, Movable {

    private Disk disk;

    private double theta;
    private float speed;

    private List<CelestialBody> orbitingBodies = new ArrayList<CelestialBody>();
    private List<Disk> orbits = new ArrayList<Disk>();

    public CelestialBody(float radius, int color) {
        this(new Point(0f, 0f), radius, color, 0, 0);
    }

    private CelestialBody(Point center, float radius, int color, float speed, double initialTheta) {
        disk = new Disk(center, radius, color, true);
        this.speed = speed;
        theta = initialTheta;
    }

    public CelestialBody addOrbiter(float radius, float distance, float speed, int color) {
        final double time = Math.random() * Math.PI * 2;
        double x = Math.cos(time), y = Math.sin(time);

        final Point p = new Point((float) x * (disk.getCircle().getRadius() + distance),
                                  (float) y * (disk.getCircle().getRadius() + distance));

        CelestialBody body = new CelestialBody(p, radius, color, speed, time);
        orbitingBodies.add(body);

        int orbitColor = (color & ~(color & 0xff000000)) | (0x55 << 24);
        orbits.add(new Disk(disk.getCircle().getCenter(), distance + disk.getCircle().getRadius(), orbitColor, false));

        return body;
    }

    @Override
    public void draw() {
        glPushMatrix();
        glRotated(theta, 0, 0, 1);

        Point center = disk.getCircle().getCenter();
        glTranslatef(center.getX(), center.getY(), 0f);

        for (Disk orbit : orbits) {
            orbit.draw();
        }

        for (CelestialBody body : orbitingBodies) {
            glPushMatrix();
            body.draw();
            glPopMatrix();
        }

        disk.draw();

        glPopMatrix();
    }

    @Override
    public void move(float dt) {
        theta += speed * dt;
        if (theta >= 360) {
            theta = 0.0;
        }

        for (CelestialBody body : orbitingBodies) {
            body.move(dt);
        }
    }
}
