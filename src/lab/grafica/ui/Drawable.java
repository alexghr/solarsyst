package lab.grafica.ui;

public interface Drawable {

    void draw();

}
