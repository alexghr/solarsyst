package lab.grafica.ui;

import lab.grafica.geometry.Circle;
import lab.grafica.geometry.Point;

import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;

public class Disk implements Drawable {

    private final int QUALITY = 16;

    private Circle circle;

    private int color;

    private boolean fill;
    private List<Point> points;
    /**
     * @param center
     * @param radius
     * @param color  ARGB format
     * @param fill if true will fill the disk, otherwise it will draw a circle
     */
    public Disk(Point center, float radius, int color, boolean fill) {
        circle = new Circle(center, radius);
        this.color = color;
        this.fill = fill;
    }

    public Circle getCircle() {
        return circle;
    }

    @Override
    public void draw() {
        if (points == null) { /* lazy load */
            init();
        }

        //glPushMatrix();
        //glTranslatef(circle.getCenter().getX(), circle.getCenter().getY(), 0);

        glBegin(fill ? GL_POLYGON : GL_LINE_LOOP);
        glColor4ub((byte) ((color >> 16) & 0xff), (byte) ((color >> 8) & 0xff), (byte) (color & 0xff),
                   (byte) ((color >> 24) & 0xff));

        for (Point point : points) {
            glVertex2f(point.getX(), point.getY());
        }

        glEnd();
        //glPopMatrix();
    }

    private void init() {
        int numPoints = (int) (QUALITY * (circle.getRadius() + 1) / 2);
        points = new ArrayList<Point>(numPoints);

        float radius = circle.getRadius();

        double dTheta = 2 * Math.PI / numPoints;
        double theta = 0;

        for (int i = 0; i < numPoints; ++i, theta += dTheta) {
            points.add(new Point((float) Math.cos(theta) * radius, (float) Math.sin(theta) * radius));
        }
    }
}
