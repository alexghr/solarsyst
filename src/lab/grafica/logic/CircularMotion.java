package lab.grafica.logic;

import lab.grafica.geometry.Point;

public class CircularMotion implements Movable {

    private Point point, center;
    private float distance;

    /**
     * Utility class for moving in circular pattern;
     * @param center the center of the trajectory
     * @param point the point to move
     * @param distance the distance between center and point
     */
    public CircularMotion(Point center, Point point, float distance) {

    }

    @Override
    public void move(float dt) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
