package lab.grafica.logic;

public interface Movable {

    void move(float dt);

}
